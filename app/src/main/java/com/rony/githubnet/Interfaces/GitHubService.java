package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.RepoData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rony on 10/23/2018.
 */

public interface GitHubService {
    @GET("/users/{user}/repos")
    Call<List<RepoData>> reposForUser(
            @Path("user") String user
    );
}
