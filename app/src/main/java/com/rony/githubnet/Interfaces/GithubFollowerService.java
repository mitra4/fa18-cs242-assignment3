package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.FollowerData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rony on 10/30/2018.
 */

public interface GithubFollowerService {
    @GET("/users/{user}/followers")
    Call<List<FollowerData>> followersForUser(
            @Path("user") String user
    );
}
