package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.FollowerData;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Rony on 10/30/2018.
 */

public interface GithubStarService {
    @PUT("/user/starred/{user}/{repo}")
    Call<FollowerData> starRepo(
            @Path("user") String user, @Path("repo") String repo, @Header("Authorization") String auth, @Header("Content-Length") int length
    );
}
