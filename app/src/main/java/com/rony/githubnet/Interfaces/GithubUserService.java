package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.UserData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rony on 10/24/2018.
 */

public interface GithubUserService {
    @GET("/users/{user}")
    Call<UserData> getUserData(
            @Path("user") String user
    );
}
