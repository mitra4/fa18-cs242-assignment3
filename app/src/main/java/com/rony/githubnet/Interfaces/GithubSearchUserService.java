package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.FollowerData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Rony on 11/5/2018.
 */

public interface GithubSearchUserService {
    @GET("/search/{users}")
    Call<List<FollowerData>> searchUser(
            @Path("users") String user, @Header("Authorization") String auth
    );
}
