package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.NotificationData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Rony on 11/5/2018.
 */

public interface GithubNotificationService {
    @GET("/notifications")
    Call<List<NotificationData>> getNotifications(
            @Header("Authorization") String auth
    );
}
