package com.rony.githubnet.Interfaces;

import com.rony.githubnet.DataFiles.FollowerData;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Rony on 10/30/2018.
 */

public interface GithubUnfollowService {
    @DELETE("/user/following/{user}")
    Call<FollowerData> unfollowUser(
            @Path("user") String user, @Header("Authorization") String auth, @Header("Content-Length") int length
    );
}