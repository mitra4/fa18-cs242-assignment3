package com.rony.githubnet.Pages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.rony.githubnet.Adapters.MyAdapter;
import com.rony.githubnet.DataFiles.DataObjects;
import com.rony.githubnet.DataFiles.RepoData;
import com.rony.githubnet.DataFiles.UserData;
import com.rony.githubnet.Interfaces.GitHubService;
import com.rony.githubnet.Interfaces.GithubUserService;
import com.rony.githubnet.R;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repositories extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView.LayoutManager mLayoutManager;
    private Context context = this;
    public String name = "mitra97";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //Get general call stuff
        String API_BASE_URL = "https://api.github.com/";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

        GitHubService client =  retrofit.create(GitHubService.class);

        Bundle b = getIntent().getExtras();
        // or other values

        if (b != null) {
            if (b.getString("newName") != null) {
                name = b.getString("newName");
            }
        }
        else {
            name = "mitra97";
        }


        // Fetch a list of the Github repositories.
        Call<List<RepoData>> call =
                client.reposForUser(name);

        // Execute the call asynchronously. Get a positive or negative callback.
        final ArrayList<DataObjects> repos = new ArrayList<>();
        call.enqueue(new Callback<List<RepoData>>() {
            @Override
            public void onResponse(Call<List<RepoData>> call, Response<List<RepoData>> response) {
                //Add all data to object, then append object to list
                Log.println(Log.INFO, "Here1", response.raw().toString());
                List<RepoData> myList=  response.body();
                for (RepoData item : myList)
                {
                    String name;
                    String description;
                    String owner;
                    String url;
                    if (item.getOwner() != null) {
                        owner = item.getOwner();
                    }
                    else
                    {
                        owner = "";
                    }
                    if (item.getDescription() != null)
                    {
                        description = (item.getDescription());
                    }
                    else
                    {
                        description = "";
                    }
                    if (item.getName() != null)
                    {
                        name = (item.getName());
                    }
                    else
                    {
                        name = "";
                    }
                    if (item.getHtml_url() != null)
                    {
                        url = item.getHtml_url();
                    }
                    else
                    {
                        url = "";
                    }

                    DataObjects data = new DataObjects(name, description, owner, url);
                    repos.add(data);
                }

                RecyclerView repoList = (RecyclerView) findViewById(R.id.RecyclerView);

                mLayoutManager = new LinearLayoutManager(context);
                repoList.setLayoutManager(mLayoutManager);

                MyAdapter adapter = new MyAdapter(repos);
                Log.e("Check adapter Set", "Adapter going to be set");
                repoList.setAdapter(adapter);
                Log.e("Check adapter Set", "Adapter Set");

            }

            @Override
            public void onFailure(Call<List<RepoData>> call, Throwable t) {
                Log.println(Log.ERROR, "Error", call.request().toString());
                Log.println(Log.ERROR, "Error", t.toString());
            }
        });


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // Inflate the menu; this adds items to the action bar if it is present.

        //Just update the menu
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);

        String API_BASE_URL = "https://api.github.com/";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

        GithubUserService client =  retrofit.create(GithubUserService.class);

        Bundle b = getIntent().getExtras();
        // or other values

        if (b != null) {
            if (b.getString("newName") != null) {
                name = b.getString("newName");
            }
        }
        else {
            name = "mitra97";
        }


        // Fetch a list of the Github repositories.
        Call<UserData> call =
                client.getUserData(name);

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                //Add all relevant info to the menu
                Log.println(Log.INFO, "Here1", response.raw().toString());
                UserData myList=  response.body();
                MenuItem repos = menu.getItem(1);
                Log.e("HERE2", repos.getTitle().toString());
                repos.setTitle("Repositories: " + myList.getPublic_repos());
                MenuItem following = menu.getItem(2);
                following.setTitle("Following: " + myList.getFollowing());
                MenuItem followers = menu.getItem(3);
                followers.setTitle("Followers: " + myList.getFollowers());
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.println(Log.ERROR, "Error", call.request().toString());
                Log.println(Log.ERROR, "Error", t.toString());
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.nav_Profile) {
            Intent newActivity = new Intent(this, MainActivity.class);
            Bundle b = new Bundle();
            b.putString("newName", name); //Your id
            newActivity.putExtras(b);
            startActivity(newActivity);

        } else if (id == R.id.nav_Repositories)
        {
            Intent newActivity = new Intent(this, Repositories.class);
            Bundle b = new Bundle();
            b.putString("newName", name); //Your id
            newActivity.putExtras(b);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Following) {
            Intent newActivity = new Intent(this, Following.class);
            Bundle b = new Bundle();
            b.putString("newName", name); //Your id
            newActivity.putExtras(b);
            startActivity(newActivity);

        } else if (id == R.id.nav_Followers) {
            Intent newActivity = new Intent(this, Followers.class);
            Bundle b = new Bundle();
            b.putString("newName", name); //Your id
            newActivity.putExtras(b);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Notifications)
        {
            Intent newActivity = new Intent(this, notifications.class);
            startActivity(newActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_Profile) {
            Intent newActivity = new Intent(this, MainActivity.class);
            startActivity(newActivity);

        } else if (id == R.id.nav_Repositories)
        {
            Intent newActivity = new Intent(this, Repositories.class);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Following) {
            Intent newActivity = new Intent(this, Following.class);
            startActivity(newActivity);

        } else if (id == R.id.nav_Followers) {
            Intent newActivity = new Intent(this, Followers.class);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Notifications)
        {
            Intent newActivity = new Intent(this, notifications.class);
            startActivity(newActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
