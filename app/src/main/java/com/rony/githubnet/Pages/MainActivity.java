package com.rony.githubnet.Pages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rony.githubnet.DataFiles.UserData;
import com.rony.githubnet.Interfaces.GithubUserService;
import com.rony.githubnet.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Create context for app
    private Context context = this;
    public String name = "mitra97";
    public String actualName;
    public String actualDate;
    public String githubName;
    public List<String> followers = new ArrayList<>();
    public List<String> following = new ArrayList<>();
    public List<String> repos = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Get all components
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Make API requests
        String API_BASE_URL = "https://api.github.com/";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

        GithubUserService client =  retrofit.create(GithubUserService.class);

        Bundle b = getIntent().getExtras();
         // or other values

        if (b != null) {
            if (b.getString("newName") != null) {
                name = b.getString("newName");
            }
        }
        else {
            name = "mitra97";
        }

        Log.println(Log.INFO, "Here0", name);
        // Fetch a list of the Github repositories.
        Call<UserData> call =
                client.getUserData(name);

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                //Get all relevant data from call
                Log.println(Log.INFO, "Here1", response.raw().toString());
                UserData myList=  response.body();
                TextView gitName = (TextView)findViewById(R.id.githubName);
                gitName.setText(myList.getLogin());
                githubName = myList.getLogin();
                TextView realName = (TextView)findViewById(R.id.realName);
                realName.setText(myList.getName());
                actualName = myList.getName();
                TextView realDate = (TextView) findViewById(R.id.profileDate);
                realDate.setText("Created at :" + myList.getCreated_at());
                actualDate = myList.getCreated_at();
                ImageView image = (ImageView) findViewById(R.id.specProfilePic);
                Picasso.with(context).load(myList.getAvatar_url()).into(image);

            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.println(Log.ERROR, "Error", call.request().toString());
                Log.println(Log.ERROR, "Error", t.toString());
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);

        //Set up call
        String API_BASE_URL = "https://api.github.com/";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

        GithubUserService client =  retrofit.create(GithubUserService.class);

        Bundle b = getIntent().getExtras();
        // or other values

        if (b != null) {
            if (b.getString("newName") != null) {
                name = b.getString("newName");
            }
        }
        else {
            name = "mitra97";
        }


        // Fetch a list of the Github repositories.
        Call<UserData> call =
                client.getUserData(name);

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                //Modify the menu view so you can see the followers/ following etc
                Log.println(Log.INFO, "Here1", response.headers().toString());
                UserData myList=  response.body();
                MenuItem repos = menu.getItem(1);
                Log.e("HERE2", repos.getTitle().toString());
                repos.setTitle("Repositories: " + myList.getPublic_repos());
                MenuItem following = menu.getItem(2);
                following.setTitle("Following: " + myList.getFollowing());
                MenuItem followers = menu.getItem(3);
                followers.setTitle("Followers: " + myList.getFollowers());
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.println(Log.ERROR, "Error", call.request().toString());
                Log.println(Log.ERROR, "Error", t.toString());
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.getString("newName") != null) {
                name = b.getString("newName");
            }
        }
        else {
            name = "mitra97";
        }

        if (id == R.id.nav_Profile) {

        } else if (id == R.id.nav_Repositories)
        {
            Intent newActivity = new Intent(this, Repositories.class);
            Bundle a = new Bundle();
            a.putString("newName", name); //Your id
            newActivity.putExtras(a);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Following) {
            Intent newActivity = new Intent(this, Following.class);
            Bundle a = new Bundle();
            a.putString("newName", name); //Your id
            newActivity.putExtras(a);
            startActivity(newActivity);

        } else if (id == R.id.nav_Followers) {
            Intent newActivity = new Intent(this, Followers.class);
            Bundle a = new Bundle();
            a.putString("newName", name); //Your id
            newActivity.putExtras(a);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Notifications)
        {
            Intent newActivity = new Intent(this, notifications.class);
            startActivity(newActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Profile) {
            Intent newActivity = new Intent(this, MainActivity.class);
            startActivity(newActivity);

        } else if (id == R.id.nav_Repositories)
        {
            Intent newActivity = new Intent(this, Repositories.class);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Following) {
            Intent newActivity = new Intent(this, Following.class);
            startActivity(newActivity);

        } else if (id == R.id.nav_Followers) {
            Intent newActivity = new Intent(this, Followers.class);
            startActivity(newActivity);
        }
        else if (id == R.id.nav_Notifications)
        {
            Intent newActivity = new Intent(this, notifications.class);
            startActivity(newActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
