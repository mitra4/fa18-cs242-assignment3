package com.rony.githubnet.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rony.githubnet.DataFiles.NotificationData;
import com.rony.githubnet.R;

import java.util.ArrayList;

/**
 * Created by Rony on 11/5/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>  {
    public Context context;
    public ImageView avatar;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Owner;
        TextView RepoName;
        TextView Reason;

        MyViewHolder(View itemView) {
            super(itemView);
            Owner = (TextView) itemView.findViewById(R.id.RepoOwner);
            context = itemView.getContext();
            RepoName = (TextView) itemView.findViewById(R.id.RepoName);
            Reason = (TextView) itemView.findViewById(R.id.Reason);


        }
    }

    private ArrayList<NotificationData> mCustomObjects;

    public NotificationAdapter(ArrayList<NotificationData> arrayList) {
        mCustomObjects = arrayList;
    }

    @Override
    public int getItemCount() {
        return mCustomObjects.size();
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_data, parent, false);
        return new NotificationAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationData object = mCustomObjects.get(position);

        String realRepoName = object.getRepo().getName();
        String realRepoOwner = object.getRepo().getOwner();
        String realReason = object.getReason();

        holder.Reason.setText(realReason);
        holder.Owner.setText(realRepoOwner);
        holder.RepoName.setText(realRepoName);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
