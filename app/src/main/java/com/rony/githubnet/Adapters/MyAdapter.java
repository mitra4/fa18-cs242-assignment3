package com.rony.githubnet.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rony.githubnet.DataFiles.DataObjects;
import com.rony.githubnet.DataFiles.FollowerData;
import com.rony.githubnet.Interfaces.GithubStarService;
import com.rony.githubnet.Interfaces.GithubUnstarService;
import com.rony.githubnet.R;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rony on 10/23/2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text1, text2, text3, text4;
        TextView star, unstar;

        MyViewHolder(View itemView) {
            super(itemView);
            text1 = (TextView) itemView.findViewById(R.id.name);
            text2 = (TextView) itemView.findViewById(R.id.description);
            text3 = (TextView) itemView.findViewById(R.id.owner);
            text4 = (TextView) itemView.findViewById(R.id.url);
            star = (TextView) itemView.findViewById(R.id.Star);
            unstar = (TextView) itemView.findViewById(R.id.Unstar);

            star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Get general call stuff
                    String API_BASE_URL = "https://api.github.com/";

                    OkHttpClient.Builder httpClient2 = new OkHttpClient.Builder();

                    Retrofit.Builder builder2 =
                            new Retrofit.Builder()
                                    .baseUrl(API_BASE_URL)
                                    .addConverterFactory(
                                            GsonConverterFactory.create()
                                    );

                    Retrofit retrofit2 =
                            builder2
                                    .client(
                                            httpClient2.build()
                                    )
                                    .build();

                    GithubStarService client =  retrofit2.create(GithubStarService.class);
                    Log.println(Log.INFO, "SERVICE WORKS", text3.getText().toString() );
                    Call<FollowerData> call = client.starRepo(text3.getText().toString(),text1.getText().toString(), "token 5c4f0b9a3d8d3bedd8306b0f01af16e8e2c58be6",0);

                    call.enqueue(new Callback<FollowerData>() {
                        @Override
                        public void onResponse(Call<FollowerData> call, retrofit2.Response<FollowerData> response) {
                            Log.println(Log.INFO, "RESPONSE", call.request().toString());
                            Log.println(Log.INFO, "RESPONSE", call.request().headers().toString());
                            Log.println(Log.INFO, "RESPONSE", response.headers().toString());
                        }

                        @Override
                        public void onFailure(Call<FollowerData> call, Throwable t) {
                            Log.println(Log.ERROR, "Error", call.request().toString());
                            Log.println(Log.ERROR, "Error", t.toString());
                        }
                    });
                }
            });

            unstar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Get general call stuff
                    String API_BASE_URL = "https://api.github.com/";

                    OkHttpClient.Builder httpClient2 = new OkHttpClient.Builder();

                    Retrofit.Builder builder2 =
                            new Retrofit.Builder()
                                    .baseUrl(API_BASE_URL)
                                    .addConverterFactory(
                                            GsonConverterFactory.create()
                                    );

                    Retrofit retrofit2 =
                            builder2
                                    .client(
                                            httpClient2.build()
                                    )
                                    .build();

                    GithubUnstarService client =  retrofit2.create(GithubUnstarService.class);
                    Log.println(Log.INFO, "SERVICE WORKS", text3.getText().toString() );
                    Call<FollowerData> call = client.unstarRepo(text3.getText().toString(),text1.getText().toString(), "token 5c4f0b9a3d8d3bedd8306b0f01af16e8e2c58be6",0);

                    call.enqueue(new Callback<FollowerData>() {
                        @Override
                        public void onResponse(Call<FollowerData> call, retrofit2.Response<FollowerData> response) {
                            Log.println(Log.INFO, "RESPONSE", call.request().toString());
                            Log.println(Log.INFO, "RESPONSE", call.request().headers().toString());
                            Log.println(Log.INFO, "RESPONSE", response.headers().toString());
                        }

                        @Override
                        public void onFailure(Call<FollowerData> call, Throwable t) {
                            Log.println(Log.ERROR, "Error", call.request().toString());
                            Log.println(Log.ERROR, "Error", t.toString());
                        }
                    });
                }
            });

        }
    }

    private ArrayList<DataObjects> mCustomObjects;

    public MyAdapter(ArrayList<DataObjects> arrayList) {
        mCustomObjects = arrayList;
    }

    @Override
    public int getItemCount() {
        return mCustomObjects.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.git_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataObjects object = mCustomObjects.get(position);

        // My example assumes CustomClass objects have getFirstText() and getSecondText() methods
        String firstText = object.getName();
        String secondText = object.getDescription();
        String thirdText = object.getOwner();
        String fourthText = object.getHtml_url();

        System.out.println(firstText + "   " + secondText + "   " + thirdText + "   " + fourthText);

        holder.text1.setText(firstText);
        holder.text2.setText(secondText);
        holder.text3.setText(thirdText);
        holder.text4.setText(fourthText);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
