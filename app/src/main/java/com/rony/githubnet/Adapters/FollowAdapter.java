package com.rony.githubnet.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rony.githubnet.DataFiles.FollowerData;
import com.rony.githubnet.Interfaces.GithubFollowService;
import com.rony.githubnet.Interfaces.GithubUnfollowService;
import com.rony.githubnet.Pages.MainActivity;
import com.rony.githubnet.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rony on 10/30/2018.
 */

public class FollowAdapter extends RecyclerView.Adapter<FollowAdapter.MyViewHolder> {

    public Context context;
    public ImageView avatar;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        TextView follow;
        TextView unfollow;

        MyViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.login);
            avatar = (ImageView) itemView.findViewById(R.id.followerPic);
            context = itemView.getContext();
            follow = (TextView) itemView.findViewById(R.id.followButton);
            unfollow = (TextView) itemView.findViewById(R.id.UnfollowButton);

            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent newActivity = new Intent(context, MainActivity.class);
                    Bundle b = new Bundle();
                    b.putString("newName", text.getText().toString()); //Your id
                    newActivity.putExtras(b);
                    context.startActivity(newActivity);
                }
            });

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Get general call stuff
                    String API_BASE_URL = "https://api.github.com/";

                    OkHttpClient.Builder httpClient2 = new OkHttpClient.Builder();

                    Retrofit.Builder builder2 =
                            new Retrofit.Builder()
                                    .baseUrl(API_BASE_URL)
                                    .addConverterFactory(
                                            GsonConverterFactory.create()
                                    );

                    Retrofit retrofit2 =
                            builder2
                                    .client(
                                            httpClient2.build()
                                    )
                                    .build();

                    GithubFollowService client =  retrofit2.create(GithubFollowService.class);
                    Log.println(Log.INFO, "SERVICE WORKS", text.getText().toString() );
                    Call<FollowerData> call = client.followUsers(text.getText().toString(), "token 5c4f0b9a3d8d3bedd8306b0f01af16e8e2c58be6",0);

                    call.enqueue(new Callback<FollowerData>() {
                        @Override
                        public void onResponse(Call<FollowerData> call, retrofit2.Response<FollowerData> response) {
                            Log.println(Log.INFO, "RESPONSE", call.request().toString());
                            Log.println(Log.INFO, "RESPONSE", call.request().headers().toString());
                            Log.println(Log.INFO, "RESPONSE", response.headers().toString());
                        }

                        @Override
                        public void onFailure(Call<FollowerData> call, Throwable t) {
                            Log.println(Log.ERROR, "Error", call.request().toString());
                            Log.println(Log.ERROR, "Error", t.toString());
                        }
                    });
                }
            });

            unfollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Get general call stuff
                    String API_BASE_URL = "https://api.github.com/";

                    OkHttpClient.Builder httpClient2 = new OkHttpClient.Builder();

                    Retrofit.Builder builder2 =
                            new Retrofit.Builder()
                                    .baseUrl(API_BASE_URL)
                                    .addConverterFactory(
                                            GsonConverterFactory.create()
                                    );

                    Retrofit retrofit2 =
                            builder2
                                    .client(
                                            httpClient2.build()
                                    )
                                    .build();

                    GithubUnfollowService client =  retrofit2.create(GithubUnfollowService.class);
                    Log.println(Log.INFO, "SERVICE WORKS", text.getText().toString() );
                    Call<FollowerData> call = client.unfollowUser(text.getText().toString(), "token 5c4f0b9a3d8d3bedd8306b0f01af16e8e2c58be6",0);

                    call.enqueue(new Callback<FollowerData>() {
                        @Override
                        public void onResponse(Call<FollowerData> call, retrofit2.Response<FollowerData> response) {
                            Log.println(Log.INFO, "RESPONSE", call.request().toString());
                            Log.println(Log.INFO, "RESPONSE", call.request().headers().toString());
                            Log.println(Log.INFO, "RESPONSE", response.headers().toString());
                        }

                        @Override
                        public void onFailure(Call<FollowerData> call, Throwable t) {
                            Log.println(Log.ERROR, "Error", call.request().toString());
                            Log.println(Log.ERROR, "Error", t.toString());
                        }
                    });
                }
            });

        }
    }

    private ArrayList<FollowerData> mCustomObjects;

    public FollowAdapter(ArrayList<FollowerData> arrayList) {
        mCustomObjects = arrayList;
    }

    @Override
    public int getItemCount() {
        return mCustomObjects.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.follower_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FollowerData object = mCustomObjects.get(position);

        // My example assumes CustomClass objects have getFirstText() and getSecondText() methods
        String firstText = object.getLogin();
        String secondText = object.getAvatar_url();


        holder.text.setText(firstText);
        Picasso.with(context).load(secondText).into(avatar);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
