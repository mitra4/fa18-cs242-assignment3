package com.rony.githubnet.DataFiles;

/**
 * Created by Rony on 10/23/2018.
 */

public class DataObjects {
    private String name;
    private String description;
    private String owner;
    private String html_url;

    public DataObjects(String name, String description, String owner, String html_url) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.html_url = html_url;
    }


    public String getName() {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
    public String getOwner()
    {
        return owner;
    }
    public String getHtml_url()
    {
        return html_url;
    }
}
