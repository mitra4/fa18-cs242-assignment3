package com.rony.githubnet.DataFiles;

/**
 * Created by Rony on 10/30/2018.
 */

public class FollowerData {
    private String login;
    private String avatar_url;
    private int id;
    private String node_id;
    private String gravatar_id;
    private String url;
    private String html_url;
    private String followers_url;
    private String following_url;
    private String gists_url;
    private String starred_url;
    private String subscriptions_url;
    private String organizations_url;
    private String repos_url;
    private  String events_url;
    private String received_events_url;
    private String type;
    private boolean site_admin;

    public FollowerData(String login, String avatar_url)
    {
        this.login = login;
        this.avatar_url = avatar_url;
    }

    public String getLogin()
    {
        return login;
    }

    public String getAvatar_url()
    {
        return avatar_url;
    }

    public int getId() {
        return id;
    }

    public String getFollowers_url() {
        return followers_url;
    }

    public String getFollowing_url() {
        return following_url;
    }

    public String getGravatar_id() {
        return gravatar_id;
    }

    public boolean isSite_admin() {
        return site_admin;
    }

    public String getGists_url() {
        return gists_url;
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getNode_id() {
        return node_id;
    }

    public String getUrl() {
        return url;
    }

    public String getEvents_url() {
        return events_url;
    }

    public String getOrganizations_url() {
        return organizations_url;
    }

    public String getReceived_events_url() {
        return received_events_url;
    }

    public String getRepos_url() {
        return repos_url;
    }

    public String getStarred_url() {
        return starred_url;
    }

    public String getSubscriptions_url() {
        return subscriptions_url;
    }

    public String getType() {
        return type;
    }
}
