package com.rony.githubnet.DataFiles;

/**
 * Created by Rony on 10/24/2018.
 */

public class UserData {
    String name;
    String login;
    int followers;
    int following;
    int public_repos;
    String created_at;
    String avatar_url;

    public String getName()
    {
        return name;
    }

    public String getLogin()
    {
        return login;
    }
    public int getFollowers()
    {
        return followers;
    }
    public int getFollowing()
    {
        return following;
    }
    public int getPublic_repos()
    {
        return public_repos;
    }
    public String getCreated_at()
    {
        return created_at;
    }
    public String getAvatar_url()
    {
        return avatar_url;
    }
}
