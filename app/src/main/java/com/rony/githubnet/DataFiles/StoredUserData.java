package com.rony.githubnet.DataFiles;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rony on 10/31/2018.
 */

public class StoredUserData {
    public String name;
    public String githubID;
    public String createdAt;
    public List<String> repos;
    public List<String> followers;
    public List<String> following;

    public StoredUserData()
    {

    }

    public StoredUserData(String name, String githubID, String createdAt, List<String> repos, List<String> followers, List<String> following)
    {
        this.name = name;
        this.githubID = githubID;
        this.createdAt = createdAt;
        this.repos = repos;
        this.followers = followers;
        this.following = following;
    }
}
