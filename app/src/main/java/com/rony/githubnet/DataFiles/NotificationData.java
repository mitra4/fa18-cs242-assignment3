package com.rony.githubnet.DataFiles;

/**
 * Created by Rony on 11/5/2018.
 */

public class NotificationData {
    public RepoData repository;
    public String reason;

    public NotificationData(RepoData repository, String reason)
    {
        this.repository = repository;
        this.reason = reason;

    }
    public RepoData getRepo()
    {
        return repository;
    }
    public String getReason()
    {
        return reason;
    }
}
