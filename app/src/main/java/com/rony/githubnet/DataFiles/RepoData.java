package com.rony.githubnet.DataFiles;

/**
 * Created by Rony on 10/23/2018.
 */


public class RepoData {
    private int id;
    private String name;
    private String description;
    private com.rony.githubnet.DataFiles.owner owner;
    private String html_url;

    public RepoData(String name, owner owner)
    {
        this.name = name;
        this.owner = owner;
    }

    public RepoData() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
    public String getOwner()
    {
        return owner.getLogin();
    }
    public String getHtml_url()
    {
        return html_url;
    }
}

